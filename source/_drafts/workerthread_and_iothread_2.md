---
title: ASP.Net 執行緒 概觀
date: 2016/11/04 11:49:17
tag:
  - .Net Framework
  - Thread Pool
  - ASP.Net
---
## 學習筆記

1. 非同步與多執行緒應用的場景並不同,不該混淆
2. 在單核的情況下,windows會分配片段的時間



## 參考
- [Threading](https://msdn.microsoft.com/en-us/library/orm-9780596527570-03-19.aspx)
- https://stackoverflow.com/questions/4844637/what-is-the-difference-between-concurrency-parallelism-and-asynchronous-methods